# Miata boost duty curve
this is with a 7psi spring
| Duty | PSI |
|------|-----|
| 10   | 7.1 |
| 15   | 7.2 |
| 20   | 8.3 |
| 25   | 8.6 |
| 30   | 9.2 |
| 40   | 10.3|
